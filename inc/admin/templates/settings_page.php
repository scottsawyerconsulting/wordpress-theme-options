<?php
/*
 * Theme options screen init
 */
function ssc_theme_options_init() {

	$sidebars = get_option( 'ssc_theme_settings_sidebars');
	if( empty( $sidebars ) ){
  	add_option( 'ssc_theme_settings_sidebars', array('sidebars'), '', 'yes');
  }
  $post_types = get_option( 'ssc_theme_settings_post_types' );
  if( empty( $post_types ) ){ 
    add_option( 'ssc_theme_settings_post_types', array('post_types'), '', 'yes');
  }
  $menus = get_option( 'ssc_theme_settings_menus' );
  if( empty( $menus ) ) {  
  	add_option( 'ssc_theme_settings_menus', array('menus'), '', 'yes');
  }
  $taxonomies = get_option( 'ssc_theme_settings_taxonomies' );
  if ( empty( $taxonomies ) ) {
  	add_option( 'ssc_theme_settings_taxonomies', array('taxonomies'), '', 'yes');
  }
  $metaboxes = get_option ( 'ssc_theme_settings_metaboxes' );
  if ( empty( $metaboxes ) ) {
  	add_option( 'ssc_theme_settings_metaboxes', array('metaboxes'), '', 'yes');
  }
}
/*
 * Theme options screen page
 */
function ssc_theme_options_add_page(){
	$option_page = add_theme_page(
		__( 'Theme Setup', 'ssc_theme' ),
		__( 'Theme setup', 'ssc_theme' ),
		'edit_theme_options',
		'theme-settings',
		'ssc_theme_options_render_page'
		);
	add_action( "load-{$option_page}", 'ssc_theme_load_settings_page' );
	add_action( 'admin_footer', 'ssc_theme_settings_js' );
/**/
}

function ssc_theme_load_settings_page() {
	if ( $_POST["ssc_settings_submit"] == 'Y' ) {
		//check_admin_referer( "ssc-theme-settings-page" );
		ssc_theme_settings_save();
		$url_parameters = isset($_GET['tab'])? 'updated=true&tab='.$_GET['tab'] : 'updated=true';
    wp_redirect( admin_url( 'themes.php?page=theme-settings&'.$url_parameters ) );
		exit;
	}
}
/*
 * Render Page
 */
function ssc_theme_options_render_page() {
	global $pagenow;
	//$settings = get_option( 'ssc_theme_settings' );
	$prefix = '_ssc_';
	if ( isset ( $_GET['tab'] ) ) $tab = $_GET['tab'];
  else $tab = 'homepage';
	ssc_theme_settings_tabs( $tab );
	// form
	echo '<form method="post" action="' . admin_url( 'themes.php?page=theme-settings&tab=' . $tab ) . '" class="ssc-settings-page ssc-settings-tab-' . $tab . '">';
  wp_nonce_field( 'ssc-theme-settings-page' );
  
  switch ( $tab ){
  	
   	case 'homepage':
   	  echo  '<h2>' . __( 'Home Page' ) . '</h2>';
   	  echo '<p>' . __( 'Configured options for this site.' ) . '</p>';
      echo '<h3>' . __( 'Sidebars' ) . '</h3>';
   	  $options = get_option('ssc_theme_settings_sidebars');  	  
      $sidebars = unserialize( $options['sidebars'] );
      if (is_array($sidebars)) {
     	  foreach ($sidebars as $sidebar => $value) {
          print '<h4>' . __( 'Sidebar #' . $sidebar ) . '</h4>';
   	    	echo '<p><strong>Machine Name</strong>: '.$value['machine_name'].'</p>';
          echo '<p><strong>Title</strong>: '.$value['title'].'</p>';
          echo '<p><strong>Description</strong>: '.$value['description'].'</p>';
          /*
   	    	print '<pre>';
   	  	  print_r($value);
   	  	  print '</pre>';
          /**/
          //echo '</p>';
          echo '<hr>';
        }
   	  }
   	  echo '<br>';
      echo '<h3>Post Types</h3>';
      //delete_option('ssc_theme_settings_post_types');
      $options = get_option( 'ssc_theme_settings_post_types' );
      $post_types = unserialize( $options['post_types'] );
      if ($post_types !== null){
     	  foreach ($post_types as $post_type => $value) {
          echo '<h4>Post type #' . $post_type . '</h4>';
          echo '<p><strong>Machine Name</strong>: '.$value['machine_name'].'</p>';
          echo '<p><strong>Labels</strong>: ';
          echo '<ul><li><strong>Name:</strong> '.$value['labels']['name'].'</li>';
          echo '<li><strong>Singular Name:</strong> '.$value['labels']['singular_name'].'</li></ul>';
          if ( $value['public'] == 1 ) {
            echo '<p><strong>Public</strong></p>';
          }
          else {
            echo '<p><strong>Private</strong></p>';
          }
          if ( $value['has_archive'] == 1 ) {
            echo '<p><strong> Archive available </strong></p>';
          }
          else {
            echo '<p><strong>No archive available</strong></p>';
          }
          echo '<p><strong>Slug: </strong>'.$value['rewrite']['slug'].'</p>';
          echo '<p><strong>Description: </strong>'.$value['description'].'</p>';
          echo '<ul>';
          for($i = 0; $i < count($value['supports']); ++$i) {
             echo '<li>'.$value['supports'][$i].'</li>';
          }
          echo '</ul>';
          echo '<hr>';
          /*
   	    	echo '<p>';

   	    	print '<pre>';
   	  	  print_r($value);
   	  	  print '</pre>';
          echo '</p>';
          /**/
        }
   	  }
      /**/
   	  echo '<br>';
      echo '<h3>' . __( 'Metaboxes' ) . '</h3>';
   	  $options = get_option( 'ssc_theme_settings_metaboxes' );
      $metaboxes = unserialize( $options['metaboxes'] );
      if (is_array($metaboxes)) {
     	  foreach ($metaboxes as $metabox => $value) {
          print '<h4>' . __( 'Metabox #' . $metabox ) . '</h4>';
   	    	echo '<p>';
   	    	print '<pre>';
   	  	  print_r($value);
   	  	  print '</pre>';
          echo '</p>';
        }
   	  }   
      echo '<br>';
      echo '<h3>' . __( 'Taxonomies' ) . '</h3>';
      $options = get_option( 'ssc_theme_settings_taxonomies' );
      $taxonomies = unserialize( $options['taxonomies'] );
      if (is_array($taxonomies)){
        foreach ($taxonomies as $taxonomy => $value) {
          echo '<h4>Tax #' . $taxonomy . '</h4>';
          echo '<p>';
          print '<pre>';
          print_r($value);
          print '</pre>';
          echo '</p>';
        }
      } 	
   	  break;
   	case 'sidebars':
   	  echo '<h2>' . __( 'Manage Sidebars' ) . '</h2>';
   	  $settings = get_option( 'ssc_theme_settings_sidebars' );
   	  $sidebars = unserialize( $settings['sidebars'] );
      if (is_array($sidebars)) {
     	  for ( $i = 0;  $i < count ( $sidebars ); ++$i ) {
       	  echo '<fieldset class="sidebar"><legend>' . __( 'Sidebar #' . $i ) . '</legend>';
       	  echo '<p><label for="sidebar[' . $i . '][machine_name]">' . __( 'Machine Name' ) . '</label>';
   	      echo '<input type="text" name="sidebar[' . $i . '][machine_name]" ';
 	    	  echo 'value="' . $sidebars[$i]['machine_name'] . '"></p>';
   	      echo '<p><label for="sidebar[' . $i . '][title]">' . __( 'Title' ) . '</label>';
   	      echo '<input type="text" name="sidebar[' . $i . '][title]" ';
 	    	  echo 'value="' . $sidebars[$i]['title'] . '"></p>';  	    
   	      echo '<p><label for=sidebar[' . $i . '][description]">' . __( 'Description' ) . '</label>';
   	      echo '<input type="text" name="sidebar[' . $i . '][description]" ';
 	    	  echo 'value="' . $sidebars[$i]['description'] . '"></p>';
   	      echo '</fieldset>';
        }
   	  }
      else {
          echo '<fieldset class="sidebar"><legend>' . __( 'Sidebar #0' ) . '</legend>';
          echo '<p><label for="sidebar[0][machine_name]">' . __( 'Machine Name' ) . '</label>';
          echo '<input type="text" name="sidebar[0][machine_name]"></p>';
          echo '<p><label for="sidebar[0][title]">' . __( 'Title' ) . '</label>';
          echo '<input type="text" name="sidebar[0][title]"></p>';       
          echo '<p><label for=sidebar[0][description]">' . __( 'Description' ) . '</label>';
          echo '<input type="text" name="sidebar[0][description]"></p>';
          echo '</fieldset>';
        }
   	  break;
   	case 'menus':
   	  echo '<h2>' . __( 'Manage Menus' ) . '</h2>';
   	  echo '<p>' . __( 'You found a feature not yet complete.' ). '</p>';
   	  break;
   	case 'post_types':
   	  echo '<h2>' . __( 'Manage Post Types' ) . '</h2>';
   	  $settings = get_option( 'ssc_theme_settings_post_types');
   	  $post_types = unserialize( $settings['post_types'] );
      if (is_array($post_types)) { // if we have post_types saved
     	  for ( $i = 0; $i < count( $post_types ); ++$i ){
     	  	echo '<fieldset class="post_type" data-count="' . $i . '"><legend>' . __( 'Post Type #' . $i ) . '</legend>';
   	    	echo '<p><label for="post_type[' . $i . '][machine_name]">' . __( 'Machine Name' ) . '</label>';
   	    	echo '<input type="text" name="post_type[' . $i . '][machine_name]" value="' . $post_types[$i]['machine_name'] . '"></p>';
   	  	  echo '<p><label for="post_type[' . $i . '][labels][name]">' . __( 'Plural Name' ) . '</label>';
     	  	echo '<input type="text" name="post_type[' . $i . '][labels][name]" value="' . $post_types[$i]['labels']['name'] . '"></p>';
     	  	echo '<p><label for="post_type[' . $i . '][labels][singular_name]">' . __( 'Singular Name' ) . '</label>';
   	    	echo '<input type="text" name="post_type[' . $i . '][labels][singular_name]" value="' . $post_types[$i]['labels']['singular_name'] . '"></p>';
   	    	echo '<fieldset><legend>' . __( 'Public' ) . '</legend>';
   	  	  echo '<p><label>' . __( 'True' ) . '</label><input type="checkbox" name="post_type[' . $i . '][public]" value="1" ' . checked( $post_types[$i]['public'], true, false) . '></p>';
          echo '</fieldset>';
          echo '<fieldset><legend>' . __( 'Has Archive' ) . '</legend>';
   	    	echo '<p><label for="post_type[' . $i . '][has_archive]">' . __( 'True' ) . '</label>';
   	    	echo '<input type="checkbox" name="post_type[' . $i . '][has_archive]" value="1" ' . checked( $post_types[$i]['has_archive'], true, false ) . '></p>';
          echo '</fieldset>';
          echo '<p><label for="post_type[' . $i . '][rewrite][slug]">' . __( 'Slug' ) . '</label>';
          echo '<input type="text" name="post_type[' . $i . '][rewrite][slug]" value="' . $post_types[$i]['rewrite']['slug'] . '"></p>';
          echo '<p><label for="post_type[' . $i . '][description]">' . __( 'Description' ) . '</label>';
          echo '<input type="text" name="post_type[' . $i . '][description]" value="' . $post_types[$i]['description'] . '"></p>';
          if ( is_array($post_types[$i]['supports'])){
            echo '<p><h3>' . __( 'Supports' ) . '</h3>';
            echo '<input type="checkbox" name="post_type[' . $i . '][supports][]" value="title" ';
          
            if ( in_array('title', $post_types[$i]['supports'] ) ){
              echo 'checked';
            }
            echo '>' . __( 'Title' ) . '<br>';
            echo '<input type="checkbox" name="post_type[' . $i . '][supports][]" value="editor" ';
            if ( in_array('editor', $post_types[$i]['supports'] ) ){
              echo 'checked';
            }
            echo '>' . __( 'Editor' ) . '<br>';
            echo '<input type="checkbox" name="post_type[' . $i . '][supports][]" value="thumbnail" ';
            if ( in_array('thumbnail', $post_types[$i]['supports'] ) ){
              echo 'checked';
            }
            echo '>' . __( 'Thumbnail' ) . '<br>';
            echo '<input type="checkbox" name="post_type[' . $i . '][supports][]" value="custom_fields" ';
            if ( in_array('custom_fields', $post_types[$i]['supports'] ) ){
              echo 'checked';
            }
            echo '>' . __( 'Custom Fields' ) . '</p>';
          }
          $taxonomies = get_taxonomies( '', 'names');
          echo '<h3>' . __( 'Taxonomies' ) . '</h3>';
          foreach( $taxonomies as $taxonomy ) {
          	echo '<input type="checkbox" name="post_type[' . $i . '][taxonomies][]" value="'. $taxonomy . '" ';
            if (is_array($post_types[$i]['taxonomies'])){
              if ( in_array( $taxonomy, $post_types[$i]['taxonomies'] ) ) {
                echo 'checked';
              }
            }
            echo '>' . $taxonomy . '<br>';
          }
          echo '<p><label for="post_type[' . $i . '][map_meta_cap]">' . __( 'Use Internal Default Meta Capability' ) . '</label>';
          echo '<input type="checkbox" name="post_type[' . $i . '][map_meta_cap]" value="1" ' . checked( $post_types[$i]['map_meta_cap'], true, false ) . ' class="map-meta-cap"></p>';
          echo '<div class="capability-type"';
          if ( $post_types[$i]['map_meta_cap'] == true ) {
          	echo 'style="display:block;"';
          }
          else {
          	echo 'style="display:none;"';
          }
          echo '>';
        	echo '<h3>' . __( 'Capability Type' ) . '</h3>';
          for ( $c = 0; $c <= count( $post_types[$i]['capability_type'] ); ++$c ) {
           	echo '<input type="text" name="post_type[' . $i . '][capability_type][' . $c . ']" value="'. $post_types[$i]['capability_type'][$c] . '"><br>';
          }        
          echo '</div>';
          echo '</fieldset>';
   	    }
      }
      else {  // if we don't have any post types saved
        echo '<fieldset class="post_type" data-count="0"><legend>' . __( 'Post Type #0' ) . '</legend>';
        echo '<p><label for="post_type[0][machine_name]">' . __( 'Machine Name' ) . '</label>';
        echo '<input type="text" name="post_type[0][machine_name]" ></p>';
        echo '<p><label for="post_type[0][labels][name]">' . __( 'Plural Name' ) . '</label>';
        echo '<input type="text" name="post_type[0][labels][name]" ></p>';
        echo '<p><label for="post_type[0][labels][singular_name]">' . __( 'Singular Name' ) . '</label>';
        echo '<input type="text" name="post_type[0][labels][singular_name]" ></p>';
        echo '<fieldset><legend>' . __( 'Public' ) . '</legend>';
        echo '<p><label>' . __( 'True' ) . '</label><input type="checkbox" name="post_type[0][public]" value="1" ></p>';
        echo '</fieldset>';
        echo '<fieldset><legend>' . __( 'Has Archive' ) . '</legend>';
        echo '<p><label for="post_type[0][has_archive]">' . __( 'True' ) . '</label>';
        echo '<input type="checkbox" name="post_type[0][has_archive]" value="1"></p>';
        echo '</fieldset>';
        echo '<p><label for="post_type[0][rewrite][slug]">' . __( 'Slug' ) . '</label>';
        echo '<input type="text" name="post_type[0][rewrite][slug]" ></p>';
        echo '<p><label for="post_type[0][description]">' . __( 'Description' ) . '</label>';
        echo '<input type="text" name="post_type[0][description]" ></p>';
        echo '<p><h3>' . __( 'Supports' ) . '</h3>';
        echo '<input type="checkbox" name="post_type[0][supports][]" value="title" >'. __( 'Title' ).'<br>';
        echo '<input type="checkbox" name="post_type[0][supports][]" value="editor">'. __( 'Editor' ).'<br>';
        echo '<input type="checkbox" name="post_type[0][supports][]" value="thumbnail">'. __( 'Thumbnail' ) .'<br>';
        echo '<input type="checkbox" name="post_type[0][supports][]" value="custom_fields">'. __( 'Custom Fields' ) .'</p>';
        $taxonomies = get_taxonomies( '', 'names');
        echo '<h3>' . __( 'Taxonomies' ) . '</h3>';
        foreach( $taxonomies as $taxonomy ) {
          echo '<input type="checkbox" name="post_type[0][taxonomies][]" value="'. $taxonomy . '" >' . $taxonomy . '<br>';
        }
        echo '<p><label for="post_type[0][map_meta_cap]">' . __( 'Use Internal Default Meta Capability' ) . '</label>';
        echo '<input type="checkbox" name="post_type[0][map_meta_cap]" value="1"  class="map-meta-cap"></p>';
        
        echo '<div class="capability-type" style="display:none;">';
        echo '<h3>'. __( 'Capability Type' ).'</h3>';
        echo '<input type="text" name="post_type[0][capability_type][]"><br>';
        echo '<input type="button" class="ssc-settings-add-capability-type" value="'. __( 'Add Capability' ) .'">';
        echo '</div>';
        echo '</fieldset>';
      }
   	  break;
   	case 'taxonomies':
   	  echo '<h2>' . __( 'Manage Taxonomies' ) . '</h2>';
   	  $settings = get_option( 'ssc_theme_settings_taxonomies' );
   	  $taxonomies = unserialize( $settings['taxonomies'] );
      if (is_array($taxonomies)) {
     	  for ( $i = 0; $i < count( $taxonomies ); ++$i ){
     	  	echo '<fieldset class="taxonomy"><legend>' . __( 'Taxonomy #' . $i ) . '</legend>';
   	    	echo '<p><label for="taxonomy[' . $i . '][machine_name]">' . __( 'Machine Name' ) . '</label>';
   	    	echo '<input type="hidden" name="taxonomy[' . $i . '][machine_name]" value="' . $taxonomies[$i]['machine_name'] . '" class="taxonomy-id">';
          echo '<input type="textbox" name="taxonomy[' . $i . '][machine_name]" value="' . $taxonomies[$i]['machine_name'] . '" class="taxonomy-id" disabled></p>';
   	  	  echo '<h3>' . __( 'Show on Post Types' ) . '</h3>';
     	  	$current_post_types = get_post_types();
     	  	foreach( $current_post_types as $pt ) { //for ( $p = 0; $p < count( $current_post_types ); ++$p ) {
   	    		echo '<input type="checkbox" name="taxonomy[' . $i . '][post_types][]" value="' . $pt . '" ';
            if ( in_array($pt, $taxonomies[$i]['post_types']) ) {
              echo 'checked="checked"';
            }
            echo '>' . $pt . '<br>';
     	  	}
   	    	echo '<h3>' . __( 'Labels' ) . '</h3>';
   	    	echo '<p><label for="taxonomy[' . $i . '][args][labels][name]">' . __( 'Taxonomy Name' ) . '</label>';
   	  	  echo '<input type="text" name="taxonomy[' . $i . '][args][labels][name]" value="' . $taxonomies[$i]['args']['labels']['name'] . '" class="taxonomy-name"></p>';
     	  	echo '<p><label for="taxonomy[' . $i . '][args][labels][singular_name]">' . __( 'Singular Name' ) . '</label>';
     	  	echo '<input type="text" name="taxonomy[' . $i . '][args][labels][singular_name]" value="' . $taxonomies[$i]['args']['labels']['singular_name'] . '" class="taxonomy-name-singular"></p>';
   	    	echo '<p><label for="taxonomy[' . $i . '][args][labels][search_items]">' . __( 'Search Categories' ) . '</label>';
   	    	echo '<input type="text" name="taxonomy[' . $i . '][args][labels][search_items]" value="' . $taxonomies[$i]['args']['labels']['search_items'] . '"></p>';
   	  	  echo '<p><label for="taxonomy[' . $i . '][args][labels][all_items]">' . __( 'All Items' ) . '</label>';
     	  	echo '<input type="text" name="taxonomy[' . $i . '][args][labels][all_items]" value="' . $taxonomies[$i]['args']['labels']['all_items'] . '"></p>';
     	  	echo '<p><label for="taxonomy[' . $i . '][args][labels][parent_item]">' . __( 'Parent Item' ) . '</label>';
   	    	echo '<input type="text" name="taxonomy[' . $i . '][args][labels][parent_item]" value="' . $taxonomies[$i]['args']['labels']['parent_item'] . '"></p>';
   	    	echo '<p><label for="taxonomy[' . $i . '][args][labels][edit_item]">' . __( 'Edit Item' ) . '</label>';
   	  	  echo '<input type="text" name="taxonomy[' . $i . '][args][labels][edit_item]" value="' . $taxonomies[$i]['args']['labels']['edit_item'] . '"></p>';
     	  	echo '<p><label for="taxonomy[' . $i . '][args][labels][update_item]">' . __( 'Update Item' ) . '</label>';
     	  	echo '<input type="text" name="taxonomy[' . $i . '][args][labels][update_item]" value="' . $taxonomies[$i]['args']['labels']['update_item'] . '"></p>';
   	    	echo '<p><label for="taxonomy[' . $i . '][args][labels][add_new_item]">' . __( 'Add New Item' ) . '</label>';
   	    	echo '<input type="text" name="taxonomy[' . $i . '][args][labels][add_new_item]" value="' . $taxonomies[$i]['args']['labels']['add_new_item'] . '"></p>';
   	  	  echo '<p><label for="taxonomy[' . $i . '][args][labels][new_item_name]">' . __( 'New Item' ) . '</label>';
     	  	echo '<input type="text" name="taxonomy[' . $i . '][args][labels][new_item_name]" value="' . $taxonomies[$i]['args']['labels']['new_item_name'] . '"></p>';
     	  	echo '<p><label for="taxonomy[' . $i . '][args][labels][menu_name]">' . __( 'Menu Name' ) . '</label>';
   	    	echo '<input type="text" name="taxonomy[' . $i . '][args][labels][menu_name]" value="' . $taxonomies[$i]['args']['labels']['menu_name'] . '"></p>';
   	    	echo '<p><label for="taxonomy[' . $i . '][args][show_ui]">' . __( 'Show UI' ) . '</label>';
   	  	  echo '<input type="checkbox" name="taxonomy[' . $i . '][args][show_ui]" value="1" ' . checked( $taxonomies[$i]['args']['show_ui'], true, false ) . '></p>';
     	  	echo '<p><label for="taxonomy[' . $i . '][args][hierarchical]">' . __( 'Hierarchical' ) . '</label>';
     	  	echo '<input type="checkbox" name="taxonomy[' . $i . '][args][hierarchical]" value="1" ' . checked( $taxonomies[$i]['args']['hierarchical'], true, false ) . '></p>';
   	    	echo '<p><label for="taxonomy[' . $i . '][args][show_admin_column]">' . __( 'Show Admin Column' ) . '</label>';
   	    	echo '<input type="checkbox" name="taxonomy[' . $i . '][args][show_admin_column]" value="1" ' . checked( $taxonomies[$i]['args']['show_admin_column'], true, false ) . '></p>';
   	  	  echo '<p><label for="taxonomy[' . $i . '][args][query_var]">' . __( 'Query Variable' ) . '</label>';
     	  	echo '<input type="checkbox" name="taxonomy[' . $i . '][args][query_var]" value="1" ' . checked( $taxonomies[$i]['args']['query_var'], true, false ) . '></p>';
     	  	echo '<p><label for="taxonomy[' . $i . '][args][public]">' . __( 'Public' ) . '</label>';
   	    	echo '<input type="checkbox" name="taxonomy[' . $i . '][args][public]" value="1" ' . checked( $taxonomies[$i]['args']['public'], true, false ) . '></p>'; 
   	    	echo '<p><label for="taxonomy[' . $i . '][args][show_in_nav_menus]">' . __( 'Show in Nav Menus' ) . '</label>';
   	  	  echo '<input type="checkbox" name="taxonomy[' . $i . '][args][show_in_nav_menus]" value="1" ' . checked( $taxonomies[$i]['args']['show_in_nav_menus'], true, false ) . '></p>'; 
     	  	echo '<p><label for="taxonomy[' . $i . '][args][show_tagcloud]">' . __( 'Show Tag Cloud' ) . '</label>';
     	  	echo '<input type="checkbox" name="taxonomy[' . $i . '][args][show_tagcloud]" value="1" ' . checked( $taxonomies[$i]['args']['show_tagcloud'], true, false ) . '></p>'; 
          echo '<h3>' . __( 'Rewrite Settings' ) . '</h3>';
          echo '<p><label for="taxonomy[' . $i . '][args][rewrite][slug]">' . __( 'Slug' ) . '</label>';
          echo '<input type="text" name="taxonomy[' . $i . '][args][rewrite][slug]" value="' . $taxonomies[$i]['args']['rewrite']['slug'] . '"></p>';
          echo '<h3>' . __( 'Capabilites' ) . '</h3>';
          echo '<input type="checkbox" name="taxonomy[' . $i . '][args][capabilites][manage_terms]" value="manage_menu"' . checked( $taxonomies[$i]['args']['capabilites']['manage_terms'], 'manage_menu', false ) . '>' . __( 'Manage Terms' ) . '<br>';
          echo '<input type="checkbox" name="taxonomy[' . $i . '][args][capabilites][edit_terms]" value="publish_menu"' . checked( $taxonomies[$i]['args']['capabilites']['edit_terms'], 'publish_menu', false ) . '>' . __( 'Edit Terms' ) . '<br>';
          echo '<input type="checkbox" name="taxonomy[' . $i . '][args][capabilites][delete_terms]" value="manage_menu"' . checked( $taxonomies[$i]['args']['capabilites']['delete_terms'], 'manage_menu', false ) . '>' . __( 'Delete Terms' ) . '<br>';
          echo '<input type="checkbox" name="taxonomy[' . $i . '][args][capabilites][assign_terms]" value="edit_menu"' . checked( $taxonomies[$i]['args']['capabilites']['assign_terms'], 'edit_menu', false ) . '>' . __( 'Assign Terms' ) . '<br>';
          echo '</fieldset>';
     	  }   	  
      }
      else { // there are no registered taxonomies
          echo '<fieldset class="taxonomy"><legend>' . __( 'Taxonomy #0' ) . '</legend>';
          echo '<p><label for="taxonomy[0][machine_name]">' . __( 'Machine Name' ) . '</label>';
          echo '<input type="hidden" name="taxonomy[0][machine_name]"  class="taxonomy-id">';
          echo '<input type="textbox" name="taxonomy[0][machine_name]" class="taxonomy-id" disabled></p>';
          echo '<h3>' . __( 'Show on Post Types' ) . '</h3>';
          $current_post_types = get_post_types();
          foreach( $current_post_types as $pt ) { //for ( $p = 0; $p < count( $current_post_types ); ++$p ) {
            echo '<input type="checkbox" name="taxonomy[0][post_types][]" value="' . $pt . '">' . $pt . '<br>';
          }
          echo '<h3>' . __( 'Labels' ) . '</h3>';
          echo '<p><label for="taxonomy[0][args][labels][name]">' . __( 'Taxonomy Name' ) . '</label>';
          echo '<input type="text" name="taxonomy[0][args][labels][name]"  class="taxonomy-name"></p>';
          echo '<p><label for="taxonomy[0][args][labels][singular_name]">' . __( 'Singular Name' ) . '</label>';
          echo '<input type="text" name="taxonomy[0][args][labels][singular_name]"  class="taxonomy-name-singular"></p>';
          echo '<p><label for="taxonomy[0][args][labels][search_items]">' . __( 'Search Categories' ) . '</label>';
          echo '<input type="text" name="taxonomy[0][args][labels][search_items]" ></p>';
          echo '<p><label for="taxonomy[0][args][labels][all_items]">' . __( 'All Items' ) . '</label>';
          echo '<input type="text" name="taxonomy[0][args][labels][all_items]" ></p>';
          echo '<p><label for="taxonomy[0][args][labels][parent_item]">' . __( 'Parent Item' ) . '</label>';
          echo '<input type="text" name="taxonomy[0][args][labels][parent_item]" ></p>';
          echo '<p><label for="taxonomy[0][args][labels][edit_item]">' . __( 'Edit Item' ) . '</label>';
          echo '<input type="text" name="taxonomy[0][args][labels][edit_item]" ></p>';
          echo '<p><label for="taxonomy[0][args][labels][update_item]">' . __( 'Update Item' ) . '</label>';
          echo '<input type="text" name="taxonomy[0][args][labels][update_item]" ></p>';
          echo '<p><label for="taxonomy[0][args][labels][add_new_item]">' . __( 'Add New Item' ) . '</label>';
          echo '<input type="text" name="taxonomy[0][args][labels][add_new_item]" ></p>';
          echo '<p><label for="taxonomy[0][args][labels][new_item_name]">' . __( 'New Item' ) . '</label>';
          echo '<input type="text" name="taxonomy[0][args][labels][new_item_name]" ></p>';
          echo '<p><label for="taxonomy[0][args][labels][menu_name]">' . __( 'Menu Name' ) . '</label>';
          echo '<input type="text" name="taxonomy[0][args][labels][menu_name]" ></p>';
          echo '<p><label for="taxonomy[0][args][show_ui]">' . __( 'Show UI' ) . '</label>';
          echo '<input type="checkbox" name="taxonomy[0][args][show_ui]" value="1" ></p>';
          echo '<p><label for="taxonomy[0][args][hierarchical]">' . __( 'Hierarchical' ) . '</label>';
          echo '<input type="checkbox" name="taxonomy[0][args][hierarchical]" value="1" ></p>';
          echo '<p><label for="taxonomy[0][args][show_admin_column]">' . __( 'Show Admin Column' ) . '</label>';
          echo '<input type="checkbox" name="taxonomy[0][args][show_admin_column]" value="1" ></p>';
          echo '<p><label for="taxonomy[0][args][query_var]">' . __( 'Query Variable' ) . '</label>';
          echo '<input type="checkbox" name="taxonomy[0][args][query_var]" value="1" ></p>';
          echo '<p><label for="taxonomy[0][args][public]">' . __( 'Public' ) . '</label>';
          echo '<input type="checkbox" name="taxonomy[0][args][public]" value="1" ></p>'; 
          echo '<p><label for="taxonomy[0][args][show_in_nav_menus]">' . __( 'Show in Nav Menus' ) . '</label>';
          echo '<input type="checkbox" name="taxonomy[0][args][show_in_nav_menus]" value="1" ></p>'; 
          echo '<p><label for="taxonomy[0][args][show_tagcloud]">' . __( 'Show Tag Cloud' ) . '</label>';
          echo '<input type="checkbox" name="taxonomy[0][args][show_tagcloud]" value="1" ></p>'; 
          echo '<h3>' . __( 'Rewrite Settings' ) . '</h3>';
          echo '<p><label for="taxonomy[0][args][rewrite][slug]">' . __( 'Slug' ) . '</label>';
          echo '<input type="text" name="taxonomy[0][args][rewrite][slug]" ></p>';
          echo '<h3>' . __( 'Capabilites' ) . '</h3>';
          echo '<input type="checkbox" name="taxonomy[0][args][capabilites][manage_terms]" value="manage_menu">' . __( 'Manage Terms' ) . '<br>';
          echo '<input type="checkbox" name="taxonomy[0][args][capabilites][edit_terms]" value="publish_menu">' . __( 'Edit Terms' ) . '<br>';
          echo '<input type="checkbox" name="taxonomy[0][args][capabilites][delete_terms]" value="manage_menu">' . __( 'Delete Terms' ) . '<br>';
          echo '<input type="checkbox" name="taxonomy[0][args][capabilites][assign_terms]" value="edit_menu">' . __( 'Assign Terms' ) . '<br>';
          echo '</fieldset>';      
      }
   	  break;
   	case 'metaboxes':
 
   	  echo '<h2>' . __( 'Manage Metaboxes' ) . '</h2>';
   	  $settings = get_option( 'ssc_theme_settings_metaboxes' );
   	  $metaboxes = unserialize( $settings['metaboxes'] );
      if (is_array($metaboxes) && count($metaboxes) > 0) {

        print '<pre>';
        print_r ( $metaboxes);
        print '</pre>';
   	    for ( $i = 0; $i < count( $metaboxes ); ++$i ){
   	  	  echo '<fieldset id="metabox-'.$i.'" class="metabox"><legend>' . __( 'Metabox #' . $i ) . '</legend>';
          //echo '<script type="text/javascript"> document.write("<input type=\"button\" class=\"ssc-settings-delete-metabox\" value=\"' . __( 'Delete Metabox' ) . '\" data-metabox=\"' .$i .'\" >" ); </script>';
     	  	echo '<p><label for="metabox[' . $i . '][id]">' . __( 'Metabox ID' ) . '</label>';
     	  	echo '<input type="text" name="metabox[' . $i . '][id]" value="' . $metaboxes[$i]['id'] . '" class="metabox-id" disabled></p>';
          echo '<input type="hidden" name="metabox[' . $i . '][id]" value="' . $metaboxes[$i]['id'] . '" class="metabox-id">';
   	    	echo '<p><label for="metabox[' . $i . '][title]">' . __( 'Title' ) . '</label>';
   	  	  echo '<input type="text" name="metabox[' . $i . '][title]" value="' . $metaboxes[$i]['title'] . '" class="metabox-title"></p>';
     	  	echo '<h3>' . __( 'Show on Post Types' ) . '</h3>';
     	  	$current_post_types = get_post_types();
   	    	foreach ( $current_post_types as $ps ) {
   	    		echo '<input type="checkbox" name="metabox[' . $i . '][pages][]" value="' . $ps . '" ';
            if ( in_array($ps, $metaboxes[$i]['pages'] ) ){
              echo ' checked="checked"';
            }
            echo '>' . $ps . '<br>';
     	  	}
   	    	echo '<h3>' . __( 'Context' ) . '</h3>';
   	      echo '<input type="radio" name="metabox[' . $i . '][context]" value="normal" ' . checked( $metaboxes[$i]['context'], 'normal', false ) . '>' . __( 'Normal' ) . '<br>';
   	      echo '<input type="radio" name="metabox[' . $i . '][context]" value="advanced" ' . checked( $metaboxes[$i]['context'], 'advanced', false ) . '>' . __( 'Advanced' ) . '<br>';
     	    echo '<input type="radio" name="metabox[' . $i . '][context]" value="side" ' . checked( $metaboxes[$i]['context'], 'side', false ) . '>' . __( 'Side' ) . '<br>';
          echo '<h3>' . __( 'Priority' ) . '</h3>';
          echo '<input type="radio" name="metabox[' . $i . '][priority]" value="high" ' . checked( $metaboxes[$i]['priority'], 'high', false ) . '>' . __( 'High' ) . '<br>';
          echo '<input type="radio" name="metabox[' . $i . '][priority]" value="core" ' . checked( $metaboxes[$i]['priority'], 'core', false ) . '>' . __( 'Core' ) . '<br>';
          echo '<input type="radio" name="metabox[' . $i . '][priority]" value="default" ' . checked( $metaboxes[$i]['priority'], 'default', false ) . '>' . __( 'Default' ) . '<br>';
          echo '<input type="radio" name="metabox[' . $i . '][priority]" value="low" ' . checked( $metaboxes[$i]['priority'], 'low', false ) . '>' . __( 'Low' ) . '<br>';
     	    echo '<h3>' . __( 'Show Names' ) . '</h3>';
   	      echo '<input type="checkbox" name="metabox[' . $i . '][show_names]" value="1" ' . checked( $metaboxes[$i]['show_names'], true, false ) . '>' . __( 'True' );
   	    	echo '<h3>' . __( 'Fields' ) . '</h3>';
   	  	  echo '<div class="ssc-settings-metabox-fields">';
     	  	for ( $f = 0; $f <= count( $metaboxes[$i]['fields'] ); ++$f ){
     	  		echo '<fieldset class="metabox-field"><legend>' . __( 'Field #' . $f ) . '</legend>';
   	    		echo '<p><label for="metabox[' . $i . '][fields][' . $f . '][name]">' . __( 'Field Name' ) . '</label>';
   	    		echo '<input type="text" name="metabox[' . $i . '][fields][' . $f . '][name]" value="' . $metaboxes[$i]['fields'][$f]['name'] . '" class="metabox-field-name"></p>';
   	  	  	echo '<p><label for="metabox[' . $i . '][fields][' . $f . '][desc]">' . __( 'Description' ) . '</label>';
   	  		  echo '<input type="text" name="metabox[' . $i . '][fields][' . $f . '][desc]" value="' . $metaboxes[$i]['fields'][$f]['desc'] . '"></p>';
     	  		echo '<p><label for="metabox[' . $i . '][fields][' . $f . '][id]">' . __( 'Field ID' ) . '</label>';
     	  		echo '<input type="text" name="metabox[' . $i . '][fields][' . $f . '][id]" value="' . str_replace( $prefix, '', $metaboxes[$i]['fields'][$f]['id'] ) . '" class="metabox-field-id" disabled>';
            echo '<input type="hidden" name="metabox[' . $i . '][fields][' . $f . '][id]" value="' . str_replace( $prefix, '', $metaboxes[$i]['fields'][$f]['id'] ) . '" class="metabox-field-id"></p>';
            $types = ssc_theme_settings_meta_field_types();
            echo '<p><label for="metabox[' . $i . '][fields][' . $f . '][type]">' . __( 'Field Type' ) . '</label>';
            echo '<select name="metabox[' . $i . '][fields][' . $f . '][type]" class="ssc-settings-metabox-field-type">';
            foreach ($types as $type_key => $type_value ) {
            	echo '<option value="' . $type_key . '" ';
            	if ( $metaboxes[$i]['fields'][$f]['type'] == $type_key ) {
            		echo 'selected';
            	}
            	echo '>'. $type_value . '</option>';
            }
            echo '</select></p>';
            echo '<div class="ssc-settings-metabox-field-params">';
            switch ( $metaboxes[$i]['fields'][$f]['type'] ){
            	case 'title':
            	  echo '<input type="hidden" name="metabox[' . $i . '][fields][' . $f . '][on_front]" value="';
            	  if ( $metaboxes[$i]['fields'][$f]['on_front'] == true ) {
            	  	echo $metaboxes[$i]['fields'][$f]['on_front'];
            	  }
            	  else {
            	  	echo '0';
          	    }
          	    echo '">';
            	  break;
            	case 'text':
            	  break;
            	case 'text_money':
            	  echo '<label for="metabox[' . $i . '][fields][' . $f . '][std]">' . __( 'Prefix' ) . '</label>';
            	  echo '<input type="text" name="metabox[' . $i . '][fields][' . $f . '][std]" value="';
          	    if ( !empty( $metaboxes[$i]['fields'][$f]['std'] ) ) {
          	    	echo $metaboxes[$i]['fields'][$f]['std'];
            	  }
            	  else{
            	  	echo '$';
            	  }
            	  echo '">';
            	  break;         	    
            	case 'multicheck':
            	  echo '<h4>' . __( 'Options' ) . '</h4>';
            	  foreach( $metaboxes[$i]['fields'][$f]['options'] as $key => $value ) {
            	  	echo '<fieldset><legend>' . __( 'Option ' . $key ) . '</legend>';
            	  	echo '<p><label for="metabox[' . $i . '][fields][' . $f . '][options]['. $key . ']">' . __( 'Option Name' ) . '</label>';
            	  	echo '<input type="text" name="metabox[' . $i . '][fields][' . $f . '][options][' . $key . ']" value="' . $metaboxes[$i]['fields'][$f]['options'][$key] . '"></p>';
          	    	echo '<p><label for="metabox[' . $i . '][fields][' . $f . '][options][' . $value . ']">' . __( 'Option Value' ) . '</label>';
          	    	echo '<input type="text" name="metabox[' . $i . '][fields][' . $f . '][options][' . $value . ']" value="' . $metaboxes[$i]['fields'][$f]['options'][$value] . '"></p></fieldset>';
            	  }
            	  break;
            	case 'checkbox':
            	  echo '<h4>' . __( 'Options' ) . '</h4>';
            	  foreach( $metaboxes[$i]['fields'][$f]['options'] as $key => $value ) {
            	  	echo '<fieldset><legend>' . __( 'Option ' . $key ) . '</legend>';
          	    	echo '<p><label for="metabox[' . $i . '][fields][' . $f . '][options]['. $key . ']">' . __( 'Option Name' ) . '</label>';
          	    	echo '<input type="text" name="metabox[' . $i . '][fields][' . $f . '][options][' . $key . ']" value="' . $metaboxes[$i]['fields'][$f]['options'][$key] . '"></p>';
          	  	  echo '<p><label for="metabox[' . $i . '][fields][' . $f . '][options][' . $value . ']">' . __( 'Option Value' ) . '</label>';
            	  	echo '<input type="text" name="metabox[' . $i . '][fields][' . $f . '][options][' . $value . ']" value="' . $metaboxes[$i]['fields'][$f]['options'][$value] . '"></p></fieldset>';
            	  }
            	  break;          	  
            	case 'select':
            	  echo '<h4>' . __( 'Options' ) . '</h4>';
            	  foreach( $metaboxes[$i]['fields'][$f]['options'] as $key => $value ) {
          	    	echo '<fieldset class="metabox[' . $i . '][fields][' . $f . '][options]" data-count="' . $key . '"><legend>' . __( 'Option ' . $key ) . '</legend>';
          	    	echo '<p><label for="metabox[' . $i . '][fields][' . $f . '][options]['. $key . '][name]">' . __( 'Option Name' ) . '</label>';
          	  	  echo '<input type="text" name="metabox[' . $i . '][fields][' . $f . '][options][' . $key . '][name]" value="' . $metaboxes[$i]['fields'][$f]['options'][$key]['name'] . '"></p>';
            	  	echo '<p><label for="metabox[' . $i . '][fields][' . $f . '][options][' . $key . '][value]">' . __( 'Option Value' ) . '</label>';
            	  	echo '<input type="text" name="metabox[' . $i . '][fields][' . $f . '][options][' . $key . '][value]" value="' . $metaboxes[$i]['fields'][$f]['options'][$key]['value'] . '"></p></fieldset>';
            	  }
            	  break;
            	case 'radio':
            	  echo '<h4>' . __( 'Options' ) . '</h4>';
          	    foreach( $metaboxes[$i]['fields'][$f]['options'] as $key => $value ) {
          	    	echo '<fieldset class="metabox[' . $i . '][fields][' . $f . '][options]" data-count="' . $key . '"><legend>' . __( 'Option ' . $key ) . '</legend>';
          	  	  echo '<p><label for="metabox[' . $i . '][fields][' . $f . '][options]['. $key . '][name]">' . __( 'Option Name' ) . '</label>';
            	  	echo '<input type="text" name="metabox[' . $i . '][fields][' . $f . '][options][' . $key . '][name]" value="' . $metaboxes[$i]['fields'][$f]['options'][$key]['name'] . '"></p>';
            	  	echo '<p><label for="metabox[' . $i . '][fields][' . $f . '][options][' . $key . '][value]">' . __( 'Option Value' ) . '</label>';
            	  	echo '<input type="text" name="metabox[' . $i . '][fields][' . $f . '][options][' . $key . '][value]" value="' . $metaboxes[$i]['fields'][$f]['options'][$key]['value'] . '"></p></fieldset>';
            	  }
            	  break;
            	case 'radio_inline':
          	    echo '<h4>' . __( 'Options' ) . '</h4>';
          	    foreach( $metaboxes[$i]['fields'][$f]['options'] as $key => $value ) {
          	  	  echo '<fieldset class="metabox[' . $i . '][fields][' . $f . '][options]" data-count="' . $key . '"><legend>' . __( 'Option ' . $key ) . '</legend>';
            	  	echo '<p><label for="metabox[' . $i . '][fields][' . $f . '][options]['. $key . '][name]">' . __( 'Option Name' ) . '</label>';
            	  	echo '<input type="text" name="metabox[' . $i . '][fields][' . $f . '][options][' . $key . '][name]" value="' . $metaboxes[$i]['fields'][$f]['options'][$key]['name'] . '"></p>';
            	  	echo '<p><label for="metabox[' . $i . '][fields][' . $f . '][options][' . $key . '][value]">' . __( 'Option Value' ) . '</label>';
            	  	echo '<input type="text" name="metabox[' . $i . '][fields][' . $f . '][options][' . $key . '][value]" value="' . $metaboxes[$i]['fields'][$f]['options'][$key]['value'] . '"></p></fieldset>';
            	  }
            	  break; 
          	  case 'taxonomy_select':
                echo '<fieldset class="metabox[' . $i . '][fields][' . $f . '][taxonomy]" >';
        		    echo '<legend>' . __( 'Choose Taxonomy' ) . '</legend>';
        		    echo '<p><select name="metabox[' . $i . '][fields][' . $f . '][taxonomy]">';
                $taxonomies = get_taxonomies( '', 'names');
                foreach( $taxonomies as $taxonomy ) {
          	      echo '<option value="' . $taxonomy . '"';
        	        if ( $metaboxes[$i]['fields'][$f]['taxonomy'] == $taxonomy ) {
        	        	echo ' selected';
        	        }
        	        echo '>' . $taxonomy . '</option>';
                }
        		    echo '</select></p></fieldset>';
              break;        	  
          	  /**/
            }
            echo '</div>'; // meta field params
            echo '</fieldset>';
     	  	}
     	  	echo '<script type="text/javascript"> document.write("<input type=\"button\" class=\"ssc-settings-add-metabox-fields\" value=\"' . __( 'Add Another Field' ) . '\" data-metabox=\"' .$i .'\" data-count=\"' . $f . '\">" ); </script>';
   	    	echo '</div>'; // meta fields
   	    	/**/
   	  	  echo '</fieldset>';
   	    }
      }   // end if there are metaboxes
      else {
        echo '<fieldset class="metabox"><legend>' . __( 'Metabox #0' ) . '</legend>';
        echo '<p><label for="metabox[0][id]">' . __( 'Metabox ID' ) . '</label>';
        echo '<input type="text" name="metabox[0][id]" class="metabox-id" disabled></p>';
        echo '<input type="hidden" name="metabox[0][id]"  class="metabox-id">';
        echo '<p><label for="metabox[0][title]">' . __( 'Title' ) . '</label>';
        echo '<input type="text" name="metabox[0][title]"  class="metabox-title"></p>';
        echo '<h3>' . __( 'Show on Post Types' ) . '</h3>';
        $current_post_types = get_post_types();
        foreach ( $current_post_types as $ps ) {
          echo '<input type="checkbox" name="metabox[0][pages][]" value="' . $ps . '">' . $ps . '<br>';
        }
        echo '<h3>' . __( 'Context' ) . '</h3>';
        echo '<input type="radio" name="metabox[0][context]" value="normal">' . __( 'Normal' ) . '<br>';
        echo '<input type="radio" name="metabox[0][context]" value="advanced">' . __( 'Advanced' ) . '<br>';
        echo '<input type="radio" name="metabox[0][context]" value="side">' . __( 'Side' ) . '<br>';
        echo '<h3>' . __( 'Priority' ) . '</h3>';
        echo '<input type="radio" name="metabox[0][priority]" value="high" >' . __( 'High' ) . '<br>';
        echo '<input type="radio" name="metabox[0][priority]" value="core" >' . __( 'Core' ) . '<br>';
        echo '<input type="radio" name="metabox[0][priority]" value="default" >' . __( 'Default' ) . '<br>';
        echo '<input type="radio" name="metabox[0][priority]" value="low" >' . __( 'Low' ) . '<br>';
        echo '<h3>' . __( 'Show Names' ) . '</h3>';
        echo '<input type="checkbox" name="metabox[0][show_names]" value="1" >' . __( 'True' );
        echo '<h3>' . __( 'Fields' ) . '</h3>';
        echo '<div class="ssc-settings-metabox-fields">';
        echo '<fieldset class="metabox-field"><legend>' . __( 'Field #0' ) . '</legend>';
        echo '<p><label for="metabox[0][fields][0][name]">' . __( 'Field Name' ) . '</label>';
        echo '<input type="text" name="metabox[0][fields][0][name]"  class="metabox-field-name"></p>';
        echo '<p><label for="metabox[0][fields][0][desc]">' . __( 'Description' ) . '</label>';
        echo '<input type="text" name="metabox[0][fields][0][desc]" ></p>';
        echo '<p><label for="metabox[0][fields][0][id]">' . __( 'Field ID' ) . '</label>';
        echo '<input type="text" name="metabox[0][fields][0][id]" " class="metabox-field-id" disabled>';
        echo '<input type="hidden" name="metabox[0][fields][0][id]" class="metabox-field-id"></p>';
        $types = ssc_theme_settings_meta_field_types();
        echo '<p><label for="metabox[0][fields][0][type]">' . __( 'Field Type' ) . '</label>';
        echo '<select name="metabox[0][fields][0][type]" class="ssc-settings-metabox-field-type">';
        foreach ($types as $type_key => $type_value ) {
          echo '<option value="' . $type_key . '" >'. $type_value . '</option>';
        }
        echo '</select></p>';
        echo '</fieldset>';
        echo '<script type="text/javascript"> document.write("<input type=\"button\" class=\"ssc-settings-add-metabox-fields\" value=\"' . __( 'Add Another Field' ) . '\" data-metabox=\"0\" data-count=\"0\">" ); </script>';
        echo '</div>'; // meta fields
        echo '</fieldset>';
      }  // end if there are not meta boxes
   	  break;
  }
  echo '<p class="submit">';
  echo '<input type="submit" class="button-primary" value="Submit">';
  echo '<input type="hidden" name="ssc_settings_submit" value="Y">';
  echo '</p>';
  echo '</form>';
}
/*
 * Save Settings
 */
function ssc_theme_settings_save() {	
	global $pagenow;
	$prefix = '_ssc_';
	if ( $pagenow == 'themes.php' && $_GET['page'] == 'theme-settings' ) {
		if ( isset( $_GET['tab'] ) ) {
			$tab = $_GET['tab'];
		}
		else {
			$tab = 'homepage';
		}
		switch ( $tab ) {
			case 'sidebars':
        foreach($_POST['sidebar'] as $sidebar) {
          if (strlen($sidebar['machine_name']) > 0 && strlen($sidebar['title']) > 0){
            $sidebars[] = $sidebar;
          }
        }
			  $settings['sidebars'] = serialize( $sidebars );
			  $updated = update_option( 'ssc_theme_settings_sidebars', $settings );
			  break;
			case 'post_types':
        foreach( $_POST['post_type'] as $post_type => $properties ) {
          if ( $properties['machine_name'] != '' ){
            $post_types[$post_type] = $properties;
            if ( $properties['public'] == 1 ) {
              $post_types[$post_type]['public'] = true;
            }
            else {
              $post_types[$post_type]['public'] = false;
            }
            if ( $properties['has_archive'] == '1' ) {
              $post_types[$post_type]['has_archive'] = true;
            }
            else {
                $post_types[$post_type]['has_archive'] = false;
            }
            if ( $properties['map_meta_cap'] == '1' ){
              $post_types[$post_type]['map_meta_cap'] = true;
            }
            else {
              $post_types[$post_type]['map_meta_cap'] = false;
            }
            foreach ( $properties['capability_type'] as $capability => $cap_val ){
              if ( $properties['capability_type'][$capability] != '' ){
                $post_types[$post_type]['capability_type'][$capability] == $cap_val;
              }
              else{
                unset( $post_types[$post_type]['capability_type'][$capability] );
              }
            }
          }
        }
			  $settings['post_types'] = serialize( $post_types );
			  $updated = update_option( 'ssc_theme_settings_post_types', $settings );
			  break;
			case 'metaboxes':
			  $metaboxes = array();
			  foreach( $_POST['metabox'] as $metabox => $properties ) {
			  	if ( $properties['id'] != '') {
			  		$metaboxes[$metabox] = $properties;
            if ( $properties['show_names'] == '1' ){
              $metaboxes[$metabox]['show_names'] = true;
            }
            else {
              $metaboxes[$metabox]['show_names'] = false;
            }
			  		foreach ( $properties['fields'] as $field => $field_values ) {
			  		  if ( $properties['fields'][$field]['name'] != '' ){
			  			  $metaboxes[$metabox]['fields'][$field] = $field_values;
			  			  $metaboxes[$metabox]['fields'][$field]['id'] = $prefix . $properties['fields'][$field]['id'];
			  			  if ( array_key_exists('options', $properties['fields'][$field])) {
			  				  foreach ( $properties['fields'][$field]['options'] as $option => $option_value){
			  				  	if ( $properties['fields'][$field]['options'][$option]['name'] == '' || $properties['fields'][$field]['options'][$option]['value'] == '' ) {
			  				  		unset( $metaboxes[$metabox]['fields'][$field]['options'][$option] );
			  				  	}
			  				  }
			  			  }
			  		  }
			  		  else {
			  				unset( $metaboxes[$metabox]['fields'][$field] );
			  			}
			  	  }
			  	}
			  }
			  $settings['metaboxes'] = serialize( $metaboxes );
			  $updated = update_option( 'ssc_theme_settings_metaboxes', $settings );
			  break;
			case 'taxonomies':
			  $taxonomies = array();
			  foreach( $_POST['taxonomy'] as $taxonomy => $properties ) {
			  	if ( $properties['machine_name'] != '') {
			  		$taxonomies[$taxonomy] = $properties;           
            if ( $properties['args']['show_ui'] == '1' ){
              $taxonomies[$taxonomy]['args']['show_ui'] = true;
            }
            else {
              $taxonomies[$taxonomy]['args']['show_ui'] = false;
            }
            if ( $properties['args']['hierarchical'] == '1' ) {
              $taxonomies[$taxonomy]['args']['hierarchical'] = true;
            }
            else {
              $taxonomies[$taxonomy]['args']['hierarchical'] = false;
            }
            if ( $properties['args']['show_admin_column'] == '1' ) {
              $taxonomies[$taxonomy]['args']['show_admin_column'] = true;
            }
            else {
              $taxonomies[$taxonomy]['args']['show_admin_column'] = false;
            }
            if ( $properties['args']['query_var'] == '1' ) {
              $taxonomies[$taxonomy]['args']['query_var'] = true;
            }
            else {
              $taxonomies[$taxonomy]['args']['query_var'] = false;
            }
            if ( $properties['args']['public'] == '1' ) {
              $taxonomies[$taxonomy]['args']['public'] = true;
            }
            else {
              $taxonomies[$taxonomy]['args']['public'] = false;
            }
            if ( $properties['args']['show_in_nav_menus'] == '1' ) {
              $taxonomies[$taxonomy]['args']['show_in_nav_menus'] = true;
            }
            else {
              $taxonomies[$taxonomy]['args']['show_in_nav_menus'] = false;
            }
            if ( $properties['args']['show_tagcloud'] == '1' ) {
              $taxonomies[$taxonomy]['args']['show_tagcloud'] = true;
            }
            else {
              $taxonomies[$taxonomy]['args']['show_tagcloud'] = false;
            }
			  	}
			  }
			  $settings['taxonomies'] = serialize( $taxonomies );
			  $updated = update_option( 'ssc_theme_settings_taxonomies', $settings );
			  break;
		}
	}
}
/*
 * Tab Control
 */
function ssc_theme_settings_tabs( $current = 'homepage' ) {		
	$tabs = array(
		'homepage' => 'Home',
		'sidebars' => 'Sidebars',
		'post_types' => 'Post Types',
		'taxonomies' => 'Taxonomies',
		'metaboxes' => 'Meta Boxes',
		);
	echo '<div id="icon-themes" class="icon32"><br></div>';
	echo '<h2 class="nav-tab-wrapper">';
	foreach( $tabs as $tab => $label ){
		$class = ( $tab == $current ) ? ' nav-tab-active' : '';
		echo '<a class="nav-tab' . $class . '" href="?page=theme-settings&tab=' . $tab . '">' . $label . '</a>';
	}
	echo '</h2>';
	/**/
}
/*
 * Theme settings validation
 */
function ssc_theme_settings_validate (){
}

function ssc_theme_settings_meta_field_types () {	
	$types = array(
		'title' => 'title',
		'text' => 'text (optionally repeatable)',
    'text_small' => 'text small (optionally repeatable)',
    'text_medium' => 'text medium (optionally repeatable)',
    'text_url' => 'text url (optionally repeatable)',
    'text_email' => 'text email (optionally repeatable)',
    'text_money' => 'text money (optionally repeatable)',
    'text_date' => 'date picker',
    'text_date_timestamp' => 'date picker (unix timestamp)',
    'text_datetime_timestamp' => 'time picker combo (unix timestamp)',
    'text_datetime_timestamp_timezone' => 'date time picker with time zone combo (serialized DateTime object)',
    'select_timezone' => 'time zone dropdown',
    'text_time' => 'time picker',
    'colorpicker' => 'color picker',
    'textarea' => 'textarea',
    'textarea_small' => 'textarea small',
    'textarea_code' => 'textarea code',
    'select' => 'select',
    'radio' => 'radio',
    'radio_inline' => 'radio inline',
    'taxonomy_radio' => 'taxonomy radio',
    'taxonomy_select' => 'taxonomy select',
    'taxonomy_multicheck' => 'taxonomy multicheck',
    'checkbox' => 'checkbox',
    'multicheck' => 'multicheck',
    'wysiwyg' => 'WYSIWYG/TinyMCE',
    'file' => 'Image/file upload',
    'file_list' => 'File List',
    'oembed' => 'oEmbed'
  );
  return $types;
}
/*
 * Theme settings js
 */
function ssc_theme_settings_js () {
	?>
	<script type="text/javascript">
    jQuery(document).ready(function($){
      $('.ssc-settings-page .submit').before('<input type="button" class="ssc-settings-add-fields" value="<?php echo  __( 'Add More' ); ?>">'  );
      $(document).on('click', '.map-meta-cap', function(){
      	$(this).parents('.post_type').find('.capability-type').toggle(this.show);
      });
      $(document).on('click', '.ssc-settings-delete-metabox', function(){
        var deleteInstance = parseInt($(this).attr('data-metabox'));
        console.log('delete' + deleteInstance);
        $('#metabox-'+deleteInstance).hide().find('input.metabox-id').val('');
      });
      $(document).on('click', '.ssc-settings-add-fields', function(){
      	var tab = getUrlVars()["tab"];     	
      	var i = $('.ssc-settings-page > fieldset').size();     	
      	switch (tab) {
      		case 'sidebars':
      		  var fields = '<fieldset class="sidebar" data-count="' + i + '"><legend>Sidebar #' + i + '</legend><p><label for="sidebar[' + i + '][machine_name]">'
      		    + '<?php echo __( 'Machine Name' ); ?></label>'
   	          + '<input type="text" name="sidebar[' + i + '][machine_name]"></p>'
   	          + '<p><label for="sidebar[' + i + '][title]"><?php echo __( 'Title' ); ?></label>'
   	          + '<input type="text" name="sidebar[' + i + '][title]"></p>'
   	          + '<p><label for=sidebar[' + i + '][description]"><?php echo __( 'Description' ); ?></label>'
   	          + '<input type="text" name="sidebar[' + i + '][description]"></p>'
   	          + '</fieldset>';
   	        break;
   	      case 'post_types':
   	        var fields =  '<fieldset class="post_type" data-count="' + i + '"><legend>Post Type #' + i + '</legend>'
   	  	      + '<p><label for="post_type[' + i + '][machine_name]"><?php echo __( 'Machine Name' ); ?></label>'
   	  	      + '<input type="text" name="post_type[' + i + '][machine_name]"></p>'
   	  	      +  '<p><label for="post_type[' + i + '][labels][name]"><?php echo __( 'Plural Name' ); ?></label>'
   	  	      +  '<input type="text" name="post_type[' + i + '][labels][name]" ></p>'
   	  	      +  '<p><label for="post_type[' + i + '][labels][singular_name]"><?php echo __( 'Singular Name' ); ?></label>'
   	  	      +  '<input type="text" name="post_type[' + i + '][label][singular_name]" ></p>'
   	  	      +  '<p><label for="post_type[' + i + '][public]"><?php echo __( 'Public' ); ?></label>'
   	  	      +  '<input type="checkbox" name="post_type[' + i + '][public]" value="1"></p>'
   	  	      +  '<p><label for="post_type[' + i + '][has_archive]"><?php echo __( 'Has Archive' ); ?></label>'
   	  	      +  '<input type="checkbox" name="post_type[' + i + '][has_archive]" value="1"></p>'
              +  '<p><label for="post_type[' + i + '][rewrite][slug]"><?php echo __( 'Slug' ); ?></label>'
              +  '<input type="text" name="post_type[' + i + '][rewrite][slug]"></p>'
              +  '<p><label for="post_type[' + i + '][description]"><?php echo __( 'Description' ); ?></label>'
              +  '<input type="text" name="post_type[' + i + '][description]"></p>'
              +  '<p><h3><?php echo __( 'Supports' ); ?></h3>'
              +  '<input type="checkbox" name="post_type[' + i + '][supports][]" value="title" ><?php echo __( 'Title' ); ?><br>'
              +  '<input type="checkbox" name="post_type[' + i + '][supports][]" value="editor"><?php echo __( 'Editor' ); ?><br>'
              +  '<input type="checkbox" name="post_type[' + i + '][supports][]" value="thumbnail"><?php echo __( 'Thumbnail' ); ?><br>'
              +  '<input type="checkbox" name="post_type[' + i + '][supports][]" value="custom_fields"><?php echo __( 'Custom Fields' ); ?></p>'
              +  '<h3><?php echo __( 'Taxonomies' ); ?></h3>'
            <?php
            $taxonomies = get_taxonomies( '', 'names');
            foreach( $taxonomies as $taxonomy ) {
            ?>
        	    + '<input type="checkbox" name="post_type[' + i + '][taxonomies][]" value="<?php echo $taxonomy; ?>"><?php echo  $taxonomy; ?><br>'
            <?php
            }
            ?>
             + '<p><label for="post_type[' + i + '][map_meta_cap]"><?php echo __( 'Use Internal Default Meta Capability' ); ?></label>'
             + '<input type="checkbox" name="post_type[' + i + '][map_meta_cap]" value="true" class="map-meta-cap"></p>'
             + '<div class="capability-type" style="display:none;">'
        	   + '<h3><?php echo __( 'Capability Type' ); ?></h3>'
        	   + '<input type="text" name="post_type[' + i + '][capability_type][]"><br>'
             + '<input type="button" class="ssc-settings-add-capability-type" value="<?php echo __( 'Add Capability' ); ?>">'
        	   + '</div>'
        	   + '</fieldset>';
            break;
          case 'metaboxes':
   	  	    var fields = '<fieldset class="metabox"><legend><?php echo __( 'Metabox #' ); ?>' + i + '</legend>'
   	  	      + '<p><label for="metabox[' + i + '][id]"><?php echo __( 'Metabox ID' ); ?></label>'
   	  	      + '<input type="text" name="metabox[' + i + '][id]" class="metabox-id" disabled></p>'
              + '<input type="hidden" name="metabox[' + i + '][id]" class="metabox-id">'
   	  	      + '<p><label for="metabox[' + i + '][title]"><?php echo __( 'Title' ); ?></label>'
   	  	      + '<input type="text" name="metabox[' + i + '][title]" class="metabox-title"></p>'
   	  	      + '<h3><?php echo __( 'Show on Post Types' ); ?></h3>'
   	  	      <?php
       	  	  $current_post_types = get_post_types();
   	      	  foreach ( $current_post_types as $ps ) {
   	      		?>
   	  		      + '<input type="checkbox" name="metabox[' + i + '][pages]" value="<?php echo $ps; ?>"><?php echo $ps; ?><br>'
   	  		      <?php
   	  	      }
   	  	      ?>
   	  	      + '<h3><?php echo __( 'Context' ); ?></h3>'
   	          + '<input type="radio" name="metabox[' + i + '][context]" value="normal"><?php echo __( 'Normal' ); ?><br>'
   	          + '<input type="radio" name="metabox[' + i + '][context]" value="advanced" ><?php echo __( 'Advanced' ); ?><br>'
   	          + '<input type="radio" name="metabox[' + i + '][context]" value="side"><?php echo __( 'Side' ); ?><br>'
   	          + '<h3><?php echo __( 'Show Names' ); ?></h3>'
   	          + '<input type="checkbox" name="metabox[' + i + '][show_names]" value="1"><?php echo __( 'True' ); ?>'
   	  	      + '<h3><?php echo __( 'Fields' ); ?></h3>'
   	  	      + '<div class="ssc-settings-metabox-fields">'
   	  		    + '<fieldset class="metabox-field"><legend><?php echo __( 'Field #0' ); ?></legend>'
   	  		    + '<p><label for="metabox[' + i + '][fields][0][name]" class="metabox-field-name"><?php echo __( 'Field Name' ); ?></label>'
   	  		    + '<input type="text" name="metabox[' + i + '][fields][0][name]"></p>'
   	  		    + '<p><label for="metabox[' + i + '][fields][0][desc]"><?php echo __( 'Description' ); ?></label>'
   	  		    + '<input type="text" name="metabox[' + i + '][fields][0][desc]"></p>'
   	  		    + '<p><label for="metabox[' + i + '][fields][0][id]"><?php echo __( 'Field ID' ); ?></label>'
   	  		    + '<input type="text" name="metabox[' + i + '][fields][0][id]" class="metabox-field-id" disabled></p>'
              + '<input type="hidden" name="metabox[' + i + '][fields][0][id]" class="metabox-field-id" >'
              + '<p><label for="metabox[' + i + '][fields][0][type]"><?php echo __( 'Field Type' ); ?></label>'
              + '<select name="metabox[' + i + '][fields][0][type]" class="ssc-settings-metabox-field-type">'
              <?php
              $types = ssc_theme_settings_meta_field_types();
              foreach ($types as $type_key => $type_value ) {
           	  ?>
      	      + '<option value="<?php echo $type_key; ?>"><?php echo $type_value; ?></option>'
       	      <?php
              }
              ?>
              + '</select></p>'
              + '<div class="ssc-settings-metabox-field-params">'
              + '</div>'
              + '</fieldset>'
   	  	      + '<input type="button" class="ssc-settings-add-metabox-fields" value="<?php echo __( 'Add Another Field' ); ?>" data-metabox="' + i + '" data-count="0">'
   	  	      + '</div>'
   	  	      + '</fieldset>';
            break;
          case 'taxonomies':
            var fields = '<fieldset class="taxonomy"><legend><?php echo __( 'Taxonomy #' ); ?>' + i + '</legend>'
   	  	      + '<p><label for="taxonomy[' + i + '][machine_name]"><?php echo __( 'Machine Name' ); ?></label>'
   	  	      + '<input type="text" name="taxonomy[' + i + '][machine_name]" class="taxonomy-id" disabled></p>'
              + '<input type="hidden" name="taxonomy[' + i + '][machine_name]" class="taxonomy-id">'
   	  	      + '<h3><?php echo __( 'Show on Post Types' ); ?></h3>'
   	  	      <?php
         	  	$current_post_types = get_post_types();
   	        	foreach ( $current_post_types as $ps ) {
   	        	?>
   	  		      + '<input type="checkbox" name="taxonomy[' + i + '][post_types][]" value="<?php echo $ps; ?>" ><?php echo $ps; ?><br>'
   	  		    <?php
   	  	      }
   	  	      ?>
   	  	      + '<h3><?php echo __( 'Labels' ); ?></h3>'
   	  	      + '<p><label for="taxonomy[' + i + '][args][labels][name]"><?php echo __( 'Taxonomy Name' ); ?></label>'
   	  	      + '<input type="text" name="taxonomy[' + i + '][args][labels][name]" class="taxonomy-name"></p>'
   	  	      + '<p><label for="taxonomy[' + i + '][args][labels][search_items]"><?php echo __( 'Search Categories' ); ?></label>'
   	  	      + '<input type="text" name="taxonomy[' + i + '][args][labels][search_items]"></p>'
   	  	      + '<p><label for="taxonomy[' + i + '][args][labels][all_items]"><?php echo __( 'All Items' ); ?></label>'
   	  	      + '<input type="text" name="taxonomy[' + i + '][args][labels][all_items]"></p>'
   	  	      + '<p><label for="taxonomy[' + i + '][args][labels][parent_item]"><?php echo __( 'Parent Item' ); ?></label>'
   	  	      + '<input type="text" name="taxonomy[' + i + '][args][labels][parent_item]"></p>'
   	  	      + '<p><label for="taxonomy[' + i + '][args][labels][edit_item]"><?php echo __( 'Edit Item' ); ?></label>'
   	  	      + '<input type="text" name="taxonomy[' + i + '][args][labels][edit_item]"></p>'
   	  	      + '<p><label for="taxonomy[' + i + '][args][labels][update_item]"><?php echo __( 'Update Item' ); ?></label>'
   	  	      + '<input type="text" name="taxonomy[' + i + '][args][labels][update_item]"></p>'
   	  	      + '<p><label for="taxonomy[' + i + '][args][labels][add_new_item]"><?php echo __( 'Add New Item' ); ?></label>'
   	  	      + '<input type="text" name="taxonomy[' + i + '][args][labels][add_new_item]"></p>'
   	  	      + '<p><label for="taxonomy[' + i + '][args][labels][new_item_name]"><?php echo __( 'New Item' ); ?></label>'
   	  	      + '<input type="text" name="taxonomy[' + i + '][args][labels][new_item_name]"></p>'
   	  	      + '<p><label for="taxonomy[' + i + '][args][labels][menu_name]"><?php echo __( 'Menu Name' ); ?></label>'
   	  	      + '<input type="text" name="taxonomy[' + i + '][args][labels][menu_name]"></p>'
   	  	      + '<p><label for="taxonomy[' + i + '][args][show_ui]"><?php echo __( 'Show UI' ); ?></label>'
   	  	      + '<input type="checkbox" name="taxonomy[' + i + '][args][show_ui]" value="1"></p>'
   	  	      + '<p><label for="taxonomy[' + i + '][args][hierarchical]"><?php echo __( 'Hierarchical' ); ?></label>'
   	  	      + '<p><label for="taxonomy[' + i + '][args][show_admin_column]"><?php echo __( 'Show Admin Column' ); ?></label>'
   	  	      + '<input type="checkbox" name="taxonomy[' + i + '][args][show_admin_column]" value="1"></p>'
   	  	      + '<p><label for="taxonomy[' + i + '][args][query_var]"><?php echo __( 'Query Variable' ); ?></label>'
   	  	      + '<input type="checkbox" name="taxonomy[' + i + '][args][query_var]" value="1"></p>'
   	  	      + '<p><label for="taxonomy[' + i + '][args][public]"><?php echo __( 'Public' ); ?></label>'
   	  	      + '<input type="checkbox" name="taxonomy[' + i + '][args][public]" value="1"></p>'
   	  	      + '<p><label for="taxonomy[' + i + '][args][show_in_nav_menus]"><?php echo __( 'Show in Nav Menus' ); ?></label>'
   	  	      + '<input type="checkbox" name="taxonomy[' + i + '][args][show_in_nav_menus]" value="1"></p>'
   	  	      + '<p><label for="taxonomy[' + i + '][args][show_tagcloud]"><?php echo __( 'Show Tag Cloud' ); ?></label>'
   	  	      + '<input type="checkbox" name="taxonomy[' + i + '][args][show_tagcloud]" value="1"></p>'
              + '<h3><?php echo __( 'Rewrite Settings' ); ?></h3>'
              + '<p><label for="taxonomy[' + i + '][args][rewrite][slug]"><?php echo __( 'Slug' ); ?></label>'
              + '<input type="text" name="taxonomy[' + i + '][args][rewrite][slug]"></p>'
              + '<h3><?php echo __( 'Capabilites' ); ?></h3>'
              + '<input type="checkbox" name="taxonomy[' + i + '][args][capabilites][manage_terms]" value="manage_categories"><?php echo __( 'Manage Terms' ); ?><br>'
              + '<input type="checkbox" name="taxonomy[' + i + '][args][capabilites][edit_terms]" value="manage_categories"><?php echo __( 'Edit Terms' ); ?><br>'
              + '<input type="checkbox" name="taxonomy[' + i + '][args][capabilites][delete_terms]" value="manage_categories"><?php echo __( 'Delete Terms' ); ?><br>'
              + '<input type="checkbox" name="taxonomy[' + i + '][args][capabilites][assign_terms]" value="edit_posts"><?php echo __( 'Assign Terms' ); ?><br>'
              + '</fieldset>';         
            break;
      	}

      	$('.ssc-settings-page > fieldset:last').after(fields);

      });
      $('.capability-type').hide().append('<input type="button" class="ssc-settings-add-capability-type" value="<?php echo __( 'Add Capability' ); ?>">' );
      $('.map-meta-cap:checked' ).parents('.post_type').find('.capability-type').show();
      $(document).on('click', '.ssc-settings-add-capability-type', function(){
        var i = $(this).parents('fieldset.post_type').attr('data-count');
        var c = $(this).parents('.capability-type').find('input[type="text"]').size();
        console.log( i );
        console.log( c );
        var fields = '<input type="text" name="post_type[' + i + '][capability_type][' + c + ']" ><br><input type="button" class="ssc-settings-add-capability-type" value="<?php echo __( 'Add Capability' ); ?>">';
        $(this).parents('.capability-type').append(fields);
        $(this).remove();
      });
      $(document).on('change', '.ssc-settings-metabox-field-type', function() {
      	var name = $(this).attr('name');
      	var type = $(this).val();
      	var field = getFieldStructure( type, name, 0 );
      	$(this).parent().next('.ssc-settings-metabox-field-params').html(field);
      });
      $(document).on('click', '.ssc-settings-meta-add-option', function(){
      	var count = parseInt($(this).attr('data-count')) + 1;
      	var type = $(this).attr('data-type');
      	var name = $(this).attr('data-name');
      	var field = getFieldStructure( type, name, count);
      	$(this).parents('.ssc-settings-metabox-field-params').append(field);
      	$(this).remove();
      });
      $(document).on('click', '.ssc-settings-add-metabox-fields', function(){
      	var f = parseInt($(this).attr('data-count'));
      	var i = parseInt($(this).attr('data-metabox'));
        var html = '<fieldset class="metabox-field"><legend><?php echo __( 'Field #' ); ?>' + f + '</legend>'
   	  		  + '<p><label for="metabox[' + i + '][fields][' + f + '][name]"><?php echo __( 'Field Name' ); ?></label>'
   	  		  + '<input type="text" name="metabox[' + i + '][fields][' + f + '][name]" class="metabox-field-name"></p>'
   	  		  + '<p><label for="metabox[' + i + '][fields][' + f + '][desc]"><?php echo __( 'Description' ); ?></label>'
   	  		  + '<input type="text" name="metabox[' + i + '][fields][' + f + '][desc]"></p>'
   	  		  + '<p><label for="metabox[' + i + '][fields][' + f  + '][id]"><?php echo __( 'Field ID' ); ?></label>'
   	  		  + '<input type="text" name="metabox[' + i + '][fields][' + f + '][id]" class="metabox-field-id" disabled></p>'
            + '<input type="hidden" name="metabox[' + i + '][fields][' + f + '][id]" class="metabox-field-id">'
            + '<p><label for="metabox[' + i + '][fields][' + f + '][type]"><?php echo __( 'Field Type' ); ?></label>'
            + '<select name="metabox[' + i + '][fields][' + ++f + '][type]" class="ssc-settings-metabox-field-type">'
          <?php
          $types = ssc_theme_settings_meta_field_types();
          foreach ($types as $type_key => $type_value ) {
          	echo '+ \'<option value="' . $type_key . '">'. $type_value . '</option>\'';
          }
          ?>
            + '</select></p>'
            + '<div class="ssc-settings-metabox-field-params">'
            + '</div>'
            + '</fieldset>'
            + '<input type="button"  class="ssc-settings-add-metabox-fields" value="<?php echo __( 'Add Another Field' ); ?>" data-metabox="' + i + '" data-count="' + f + '">';
        $(this).parents('.ssc-settings-metabox-fields').append(html);
        $(this).remove();

      });
      $(document).on('keyup change', 'fieldset.metabox input.metabox-title', function(){
      	var current_title = $(this).val();
      	var safeString = machineSafe(current_title);
      	$(this).parents('fieldset.metabox').find('input.metabox-id').val(safeString);
      });
      $(document).on('keyup change', 'fieldset.metabox-field input.metabox-field-name', function(){
      	var current_title = $(this).val();
      	var safeString = machineSafe(current_title);
      	$(this).parents('fieldset.metabox-field').find('input.metabox-field-id').val(safeString);
      });
      $(document).on('keyup change', 'fieldset.taxonomy input.taxonomy-name', function(){
      	var current_title = $(this).val();
      	var safeString = machineSafe(current_title);
      	$(this).parents('fieldset.taxonomy').find('input.taxonomy-id').val(safeString);
      });      
      function machineSafe( originalString ) {
      	originalString = originalString.toLowerCase().replace(/\s/g, '_').replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|\/\@\!\#\%\^\&\;\:\'\"]/g, '');
      	return originalString;
      }
      function getUrlVars() {
        var vars = {};
        var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
        vars[key] = value;
      });
      return vars;
      }
      function getFieldStructure( type, id, count ) {
      	var field = '';
      	switch (type){
      		case 'title':
      		  break;
      		case 'text_money':
      		  id = id.replace("type", "std");
      		  field = '<p><label for="' + id + '"><?php echo __( 'Prefix' ); ?></label>'
      		    + '<input type="text" name="' + id + '" value="$"></p>';
      		  break;
      		case 'colorpicker':
      		  id = id.replace("type", "std");
      		  field = '<p><label for="' + id + '"><?php echo __( 'Default Color' ); ?></label>'
      		    + '<input type="text" class="cmb_colorpicker" name="' + id + '" value="#ffffff"></p>';
      		  break;
          case 'textarea':
            id = id.replace("type", "std");
            field = '<p><label for="' + id + '"><?php echo __( 'Standard Value ( optional )' ); ?></label>'
              + '<input type="text" name="' + id + '" ></p>';
            break;
          case 'textarea_small':
            id = id.replace("type", "std");
            field = '<p><label for="' + id + '"><?php echo __( 'Standard Value ( optional )' ); ?></label>'
              + '<input type="text" name="' + id + '" ></p>';
            break;
          case 'textarea_code':
            id = id.replace("type", "std");
            field = '<p><label for="' + id + '"><?php echo __( 'Standard Value ( optional )' ); ?></label>'
              + '<input type="text" name="' + id + '" ></p>';
            break;  
      		case 'select':
      		  id = id.replace("type", "options");
      		  var button = '<input type="button" value="<?php echo __( 'Add Another Option' ); ?>" class="ssc-settings-meta-add-option" data-name="' + id + '" data-type="select"  data-count="' + count + '" >';
      		  var fieldset = '<fieldset class="' + id + '" data-count="' + count + '">';
      		  id = id + "["+count+"]";
      		  field = fieldset + '<legend><?php echo __( 'Option ' ); ?>' + count + '</legend>'
      		    + '<p><label for="' + id + '[name]"><?php echo __( 'Option Name' ); ?></label>'
      		    + '<input type="text" name="' + id + '[name]"></p>'
      		    + '<p><label for="' + id + '[value]"><?php echo __( 'Option Value' ); ?></label>'
      		    + '<input type="text" name="' + id + '[value]"></p></fieldset>'
      		    + button;
            break;
      		case 'multicheck':
      		  id = id.replace("type", "options");
      		  var button = '<input type="button" value="<?php echo __( 'Add Another Option' ); ?>" class="ssc-settings-meta-add-option" data-name="' + id + '" data-type="multicheck"  data-count="' + count + '" >';
      		  var fieldset = '<fieldset class="' + id + '" data-count="' + count + '">';
      		  id = id + "["+count+"]";
      		  field = fieldset + '<legend><?php echo __( 'Option ' ); ?>' + count + '</legend>'
      		    + '<p><label for="' + id + '[name]"><?php echo __( 'Option Name' ); ?></label>'
      		    + '<input type="text" name="' + id + '[name]"></p>'
      		    + '<p><label for="' + id + '[value]"><?php echo __( 'Option Value' ); ?></label>'
      		    + '<input type="text" name="' + id + '[value]"></p></fieldset>'
      		    + button;
            break;
      		case 'checkbox':
      		  id = id.replace("type", "options");
      		  var button = '<input type="button" value="<?php echo __( 'Add Another Option' ); ?>" class="ssc-settings-meta-add-option" data-name="' + id + '" data-type="multicheck"  data-count="' + count + '" >';
      		  var fieldset = '<fieldset class="' + id + '" data-count="' + count + '">';
      		  id = id + "["+count+"]";
      		  field = fieldset + '<legend><?php echo __( 'Option ' ); ?>' + count + '</legend>'
      		    + '<p><label for="' + id + '[name]"><?php echo __( 'Option Name' ); ?></label>'
      		    + '<input type="text" name="' + id + '[name]"></p>'
      		    + '<p><label for="' + id + '[value]"><?php echo __( 'Option Value' ); ?></label>'
      		    + '<input type="text" name="' + id + '[value]"></p></fieldset>'
      		    + button;
            break;            
          case 'radio':
      		  id = id.replace("type", "options");
      		  var button = '<input type="button" value="<?php echo __( 'Add Another Option' ); ?>" class="ssc-settings-meta-add-option" data-name="' + id + '" data-type="radio"  data-count="' + count + '" >';
      		  var fieldset = '<fieldset class="' + id + '" data-count="' + count + '">';
      		  id = id + "["+count+"]";
      		  field = fieldset + '<legend><?php echo __( 'Option ' ); ?>' + count + '</legend>'
      		    + '<p><label for="' + id + '[name]"><?php echo __( 'Option Name' ); ?></label>'
      		    + '<input type="text" name="' + id + '[name]"></p>'
      		    + '<p><label for="' + id + '[value]"><?php echo __( 'Option Value' ); ?></label>'
      		    + '<input type="text" name="' + id + '[value]"></p></fieldset>'
      		    + button;
            break;
          case 'taxonomy_select':
            id = id.replace("type", "taxonomy");
      		  var fieldset = '<fieldset class="' + id + '" >';
      		  field = fieldset + '<legend><?php echo __( 'Choose Taxonomy' ); ?></legend>'
      		    + '<p><select name="' + id + '">'
            <?php
            $taxonomies = get_taxonomies( '', 'names');
            foreach( $taxonomies as $taxonomy ) {
            ?>
        	    + '<option value="<?php echo $taxonomy; ?>"><?php echo  $taxonomy; ?></option>'
            <?php
            }
            ?>
      		    + '</select></p></fieldset>';
            break;
          case 'taxonomy_multicheck':
            id = id.replace("type", "taxonomy");
      		  var fieldset = '<fieldset class="' + id + '" >';
      		  field = fieldset + '<legend><?php echo __( 'Choose Taxonomy' ); ?></legend>'
      		    + '<p><select name="' + id + '">'
            <?php
            $taxonomies = get_taxonomies( '', 'names');
            foreach( $taxonomies as $taxonomy ) {
            ?>
        	    + '<option value="<?php echo $taxonomy; ?>"><?php echo  $taxonomy; ?></option>'
            <?php
            }
            ?>
      		    + '</select></p></fieldset>';
            break;
          case 'taxonomy_radio':
            id = id.replace("type", "taxonomy");
      		  var fieldset = '<fieldset class="' + id + '" >';
      		  field = fieldset + '<legend><?php echo __( 'Choose Taxonomy' ); ?></legend>'
      		    + '<p><select name="' + id + '">'
            <?php
            $taxonomies = get_taxonomies( '', 'names');
            foreach( $taxonomies as $taxonomy ) {
            ?>
        	    + '<option value="<?php echo $taxonomy; ?>"><?php echo  $taxonomy; ?></option>'
            <?php
            }
            ?>
      		    + '</select></p></fieldset>';
            break;
          case 'file':
            id = id.replace("type", "file");
      		  var fieldset = '<fieldset class="' + id + '" >';
      		  field = fieldset + '<legend><?php echo __( 'File Options' ); ?></legend>'
      		    + '<p><label for="' + id + '[save_id]"><?php echo __( 'Save Id' ); ?></label>'
      		    + '<input type="text" name="' + id + '[save_id]" value="true"></p>'
      		    + '<h3><?php echo __( 'Allow' ); ?></h3>'
      		    + '<input type="checkbox" name="' + id + '[allow]" value="url"><?php echo __( 'URL' ); ?><br>'
      		    + '<input type="checkbox" name="' + id + '[allow]" value="attachment"><?php echo __( 'Attachment' ); ?></p></fieldset>';
            break;
          case 'wysiwyg':
            id = id.replace("type", "options");
            var fieldset = '<fieldset class="' + id + '" >';
            field = fieldset + '<legend><?php echo __( 'WYSIWYG Options' ); ?></legend>'
              + '<p><label for="' + id + '[wpautop]"><?php echo __( 'Use WPAutop?' ); ?></label>'
              + '<input type="checkbox" name="' + id + '[wpautop]" value="1"></p>'
              + '<p><label for="' + id + '[media_buttons]"><?php echo __( 'Show insert/ upload buttons' ); ?></label>'
              + '<input type="checkbox" name="' + id + '[media_buttons]" value="1"></p>'
              + '<p><label for="' + id + '[textarea_name]"><?php echo __( 'Textarea Name (  set the textarea name to something different, square brackets [] can be used here )' ); ?></label>'
              + '<input type="text" name="' + id + '[textarea_name]" value="<?php echo $editor_id; ?>"></p>'
              + '<p><label for="' + id + '[textarea_rows]"><?php echo __( 'Textarea Rows ( rows="..." )' ); ?></label>'
              + '<input type="text" name="' + id + '[textarea_rows]" value="<?php echo get_option('default_post_edit_rows', 10); ?>"></p>'
              + '<p><label for="' + id + '[tabindex]"><?php echo __( 'Tab Index' ); ?></label>'
              + '<input type="text" name="' + id + '[tabindex]" ></p>'
              + '<p><label for="' + id + '[editor_css]"><?php echo __( 'extra styles for both visual and HTML editors buttons, needs to include the &lt;style&gt; tags, can use "scoped"' ); ?></label>'
              + '<textarea name="' + id + '[editor_css]"></textarea></p>'
              + '<p><label for="' + id + '[editor_class]"><?php echo __( 'add extra class(es) to the editor textarea' ); ?></label>'
              + '<input type="text" name="' + id + '[editor_class]"></p>'
              + '<p><label for="' + id + '[teeny]"><?php echo __( 'output the minimal editor config used in Press This' ); ?></label>'
              + '<input type="checkbox" name="' + id + '[teeny]" value="1"></p>'
              + '<p><label for="' + id + '[dfw]"><?php echo __( 'replace the default fullscreen with DFW (needs specific css)' ); ?></label>'
              + '<input type="checkbox" name="' + id + '[dfw]" value="1"></p>'
              + '<p><label for="' + id + '[tinymce]"><?php echo __( ' load TinyMCE, can be used to pass settings directly to TinyMCE using an array()' ); ?></label>'
              + '<input type="checkbox" name="' + id + '[tinymce]" value="1" checked="checked"></p>'
              + '<p><label for="' + id + '[quicktags]"><?php echo __( 'load Quicktags, can be used to pass settings directly to Quicktags using an array() ' ); ?></label>'
              + '<input type="checkbox" name="' + id + '[quicktags]" value="1" checked="checked"></p>'
              + '</fieldset>';
            break;
      	}
      	return field;
      }

    });
  </script>
<?php
/**/
}
?>