<?php

if ( file_exists( dirname( __FILE__ ) . '/mb/init.php' ) ) {
	require_once dirname( __FILE__ ) . '/mb/init.php';
} elseif ( file_exists( dirname( __FILE__ ) . '/MB/init.php' ) ) {
	require_once dirname( __FILE__ ) . '/MB/init.php';
}

add_action( 'cmb2_init', 'ssc_co_register_header_metabox' );
/**
 * Hook in and add a demo metabox. Can only happen on the 'cmb2_init' hook.
 */

function ssc_co_register_header_metabox() {	
	$prefix = '_ssc_co_';

	$cmb_demo = new_cmb2_box( array(
		'id'            => $prefix . 'banner_box',
		'title'         => __( 'Banner Image', 'cmb2' ),
		'object_types'  => array( 'page', 'post', 'services' ),
		 'context'    => 'normal',
		 'priority'   => 'default',
		 'show_names' => true, // Show field names on the left
		 'cmb_styles' => true, // false to disable the CMB stylesheet
	) );

	$cmb_demo->add_field( array(
		'name' => __( 'Banner Image', 'cmb2' ),
		'desc' => __( '', 'cmb2' ),
		'id'   => $prefix . 'banner_img',
		'type' => 'file',
	) );
	$cmb_demo->add_field( array(
		'name'    => __( 'Banner Text Box Background Color', 'cmb2' ),
		'desc'    => __( '', 'cmb2' ),
		'id'      => $prefix . 'banner_bg',
		'type'    => 'colorpicker',
		'default' => '',
	) );
	$cmb_demo->add_field( array(
		'name'    => __( 'Banner Text Color', 'cmb2' ),
		'desc'    => __( '', 'cmb2' ),
		'id'      => $prefix . 'banner_color',
		'type'    => 'colorpicker',
		'default' => '',
	) );
	$cmb_demo->add_field( array(
		'name' => __( 'Banner Text Line 1', 'cmb2' ),
		'desc' => __( '', 'cmb2' ),
		'id'   => $prefix . 'banner_txt_1',
		'type' => 'text',
	) );

	$cmb_demo->add_field( array(
		'name' => __( 'Banner Text Line 2', 'cmb2' ),
		'desc' => __( '', 'cmb2' ),
		'id'   => $prefix . 'banner_txt_2',
		'type' => 'text',
	) );

	$cmb_demo->add_field( array(
		'name' => __( 'Banner Read More Text', 'cmb2' ),
		'desc' => __( '', 'cmb2' ),
		'id'   => $prefix . 'banner_read_more',
		'type' => 'text',
	) );
	$cmb_demo->add_field( array(
		'name' => __( 'Banner Read More Text', 'cmb2' ),
		'desc' => __( '', 'cmb2' ),
		'id'   => $prefix . 'banner_read_more',
		'type' => 'text',
	) );
	$cmb_demo->add_field( array(
		'name'    => __( 'Banner Read More Text Color', 'cmb2' ),
		'desc'    => __( '', 'cmb2' ),
		'id'      => $prefix . 'banner_read_more_color',
		'type'    => 'colorpicker',
		'default' => '',
	) );
	$cmb_demo->add_field( array(
		'name'    => __( 'Banner Read More Background Color', 'cmb2' ),
		'desc'    => __( '', 'cmb2' ),
		'id'      => $prefix . 'banner_read_more_bg',
		'type'    => 'colorpicker',
		'default' => '',
	) );
	$cmb_demo->add_field( array(
		'name' => __( 'Banner Link', 'cmb2' ),
		'desc' => __( '', 'cmb2' ),
		'id'   => $prefix . 'banner_link',
		'type' => 'text',
	) );
}