<?php
/**
 * Plugin Name: SSC Theme Settings
 * Plugin URI: https://bitbucket.org/scottsawyerconsulting/
 * Description: Create custom theme settings in the WP UI
 * Version: 0.1.0
 * Author: Scott Sawyer
 * Author URI: http://scottsawyerconsulting.com/about/team/scottsawyer
 */

require_once( 'inc/functions-inc.php' ); 
?>