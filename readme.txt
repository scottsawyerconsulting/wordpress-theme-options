=== Plugin Name ===
Contributors: scottsawyer, kylebaker, benbowen 
Donate link: http://scottsawyerconsulting.com/about
Tags: SSC Theme Options
Requires at least: 3.0
Tested up to: 4.0
Stable tag: 4.3

SSC Theme Options.  Allows for creation of custom settings in WordPress Themes.

== Description ==

Adds settings screens that allow creation of custom post types, meta boxes ( and fields ), custom taxonomies and more.

**This plugin comes with NO WARRANTY and I am no way associated with Google or any of their products or services.**
*Use this plugin at your own risk**
== Installation ==


1. Upload `ssc-theme-options` to the `/wp-content/plugins/` directory
2. Activate the plugin through the 'Plugins' menu in WordPress
3. A new menu item appears under "Appearance" in your Dashboard called "Theme Setup" wp-admin/themes.php?page=theme-settings


== Frequently Asked Questions ==

= How do I use my custom post types in my theme =

By default, custom post types will use your single.php theme file.  You may want to create a new single for the post type.  Use the machine name in the file name ( single-[my_post_type].php )

= Can custom taxonomies be added to existing post types? =

Yes.

== Screenshots ==

1. This screen shot description corresponds to screenshot-1.



== Changelog ==

= 0.1.0 = 
Initial release


== Arbitrary section ==

== TODO == 
Clean up the display of created theme settings.  Currently, the entire object is dumped on the screen.
Clean up the theme settings forms.  